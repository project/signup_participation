
The signup participation module provides a signup pane with a radio option with "Yes", "No" or "Maybe". This allows users to quickly set there participation to a signup event.

Requires at least 6.x-2.x version of signup! It doesn't work with 6.x-1.x.
